This field type lets the user enter the URL for an RSS enclosure. It can be used to create podcasts of files stored on other sites.

Darren Oh